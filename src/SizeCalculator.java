/*
 * Name: Durjoy Barua
 * Section: Intro to programming 300
 * Lab Instructor: Rejaul Chowdhury
 */

import java.text.DecimalFormat;

public class SizeCalculator {
	private Box firstBox;  // First box to compare
	private Box secondBox; // Second box to compare
	private String message;  // Message to be displayed
	private DecimalFormat df = new DecimalFormat("#0.00"); // Decimal format to be used
	
	
	
	public SizeCalculator() {
		// Default (no-arg) constructor
		firstBox = new Box();
		secondBox = new Box();
	}
	public void inputBoxes() {  // Input the two boxes
		System.out.println("Size Calculator - We Speak volumes\n");
		
		System.out.println("Enter the first box dimensions ");
		inputBox(firstBox);
		
		System.out.println("Enter the second box dimensions ");
		inputBox(secondBox);
		
	}
	private void inputBox(Box box) {
		// (Optional) Method to simplify box entry
		box.inputLength();
		box.inputWidth();
		box.inputHeight();
	}
	public void calculateSizes() {
		// Calculate the difference between the two boxes and store the value in message
		if(firstBox.calcVolume() == secondBox.calcVolume()) {
			message = "The first box is the same size as the second box";
		}else if(firstBox.calcVolume() > secondBox.calcVolume()) {
			if(firstBox.calcVolume() < 2*secondBox.calcVolume()) {
				message = "The first box is slightly bigger than the second box";
			}else {
				int timesThesize = (int)Math.round(firstBox.calcVolume())/(int)Math.round(secondBox.calcVolume());
				
				message = "The first box is "+timesThesize+" times bigger than the second box";
			}
		}else {
			if(secondBox.calcVolume() < 2*firstBox.calcVolume()) {
				message = "The Second box is slightly bigger than the First box";
			}else {
				int timesThesize = (int)Math.round(secondBox.calcVolume())/(int)Math.round(firstBox.calcVolume());
				
				message = "The Second box is "+timesThesize+" times bigger than the first box";
			}
		}
	}
	public void display() {
		// Display the message
		System.out.print("\nFirst box :");
		firstBox.displayDimensions();
		System.out.print(" Volume = "+df.format(firstBox.calcVolume()));
		
		System.out.print("\nSecond box :");
		secondBox.displayDimensions();
		System.out.print(" Volume = "+df.format(secondBox.calcVolume()));
		
		System.out.println("\n"+message);
	}
}
