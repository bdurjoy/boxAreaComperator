/*
 * Name: Durjoy Barua
 * Section: Intro to programming 300
 * Lab Instructor: Rejaul Chowdhury
 */
import java.util.Scanner;

public class Box {
	private double length;  // Length value
	private double width;  // Width value
	private double height; // Height value
	private Scanner input = new Scanner(System.in); // Common scanner for all input
	
	
	
	public Box() {
		// Default (no-arg) constructor
		length = 1.00;
		width = 1.00;
		height = 1.00;
	}
	public Box(double l,double w,double h) {
		// Initial constructor
		length = l;
		width = w;
		height = h;
	}
	public Box(Box box) {
		// Copy constructor
		length = box.length;
		width = box.width;
		height = box.height;
	}
	public void inputLength() {
		// Input length value
		System.out.println("Enter Length :");
		length = input.nextDouble();
	}
	public void inputWidth() {
		// Input width value
		System.out.println("Enter Width :");
		width = input.nextDouble();
	}
	public void inputHeight() {
		// Input height value
		System.out.println("Enter Height :");
		height = input.nextDouble();
	}
	public void displayDimensions() {
		// Display the box in the proper format
		System.out.print(length+" x "+width+" x "+height);
	}
	public double calcVolume() {
		// Calculate the box volume
		return (length*width*height);
	}
}
