/*
 * Name: Durjoy Barua
 * Section: Intro to programming 300
 * Lab Instructor: Rejaul Chowdhury
 */
public class Assign2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		SizeCalculator size = new SizeCalculator();

		size.inputBoxes();
		
		size.calculateSizes();
		
		size.display();
	}

}
